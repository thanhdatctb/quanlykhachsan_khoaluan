﻿use master
go
if (exists(select * from sysdatabases where name = 'QuanLyKhachSan'))
drop database QuanLyKhachSan
go
create database QuanLyKhachSan
go 
use QuanLyKhachSan
go
create table admins
(
	id int primary key identity(1,1),
	userName varchar(100),
	hashpass varchar(100),
	adminName nvarchar(100)
)

go
create table customers
(
	id int primary key identity(1,1),
	userName varchar(100) unique,
	hashpass varchar(100),
	fullName nvarchar(1000),
	gender nvarchar(10),
	customerNational varchar(100),
	dob datetime,
	noiSinh nvarchar(100),
	DiaChi nvarchar(100),
	Phone varchar(11),
	mail varchar(100) ,
	IDCard varchar(20),
	IDPlace nvarchar(1000),
	passport varchar(20),
	passportPlace varchar(100),
	img varchar(100),
	account float,
	status int --- 0: da xoa, 1: ok
)
go
create table transactions
(
	id int primary key identity(1,1),
	transactionDate datetime,
	TransactionType bit,
	paymentMethod nvarchar(100),
	customerId int foreign key references customers(id)
)
go

create table roomTypes
(
	id int primary key identity(1,1), 
	typeName nvarchar(100),
)
select * from roomTypes
go
create table rooms
(
	roomNumber int primary key,
	roomTypeId int foreign key references roomTypes(id),
	descripiton text,
	imageLink nvarchar(100),
	area float,
	status bit,
	pricePernight float,
)
go

go
create table orderstatuses
(
	id int primary key identity(1,1),
	name varchar(100)
)

go
create table orders
(
	id int primary key identity(1,1),
	customerId int foreign key references customers(id),
	roomNumber int foreign key references  rooms(roomNumber),
	beginDate datetime,
	endDate datetime,
	orderStatusId int foreign key references orderstatuses(id),
	total float,
	deposit float,
	
)
go
create table payment
(
	id int primary key identity(1,1),
	orderId int foreign key references orders(id),
	paymentMethod nvarchar(100),
	paymentTime datetime,
)

go
---username: admin, password: admin, hash: sha1
insert into admins (userName,hashpass,adminName)values 
( 'admin', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'Admin'),
('admin2', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'Admin2')
go
--username:cus1, password: abc
insert into customers (userName,hashpass,fullName,gender,customerNational,dob,noiSinh,DiaChi,Phone,mail,IDCard,IDPlace,passport,passportPlace,img,account, status) values
('cus1','a9993e364706816aba3e25717850c26c9cd0d89d', N'Nguyễn Văn A', 'Nam', 'Vietnam', '12-12-1991', 'Viet nam', 'Ha noi', '0912345678','abc@gmail.com', '212121', 'vietnam', '121', N'Mỹ','hotel-logo-icon-png-favpng-MBQ66CMrwQiX4JLnTGi2Hz4Yn.jpg',0.0, 1)
go
insert into roomTypes (typeName) values (N'Phòng Vip'),
									(N'Phòng Thường')
go
insert into dbo.orderstatuses (name) values
(N'Đã thanh toán'),
(N'Chờ thanh toán'),
(N'Tranh chấp'),
(N'Yêu cầu hoàn tiền'),
(N'Hoàn tiền một phần')
go
insert into rooms values
(101, 1, N'Phòng vip tốt', 'caractere-2511.jpg', 25, 1, 150000),
(102, 2, N'Phòng thường tốt', 'caractere-2511.jpg', 20, 1, 100000)
go
select * from customers
select id,userName,hashpass,fullName,
CASE
    WHEN gender = 0 THEN 'Nam'
    WHEN gender = 1 THEN N'Nữ'
END AS gender,
customerNational,dob,noiSinh,DiaChi,Phone,mail,IDCard,IDPlace,passport,passportPlace,account from customers