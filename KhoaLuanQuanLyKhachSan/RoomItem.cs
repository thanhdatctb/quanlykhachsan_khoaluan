﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using KhoaLuanQuanLyKhachSan.Model;

namespace KhoaLuanQuanLyKhachSan
{
    public partial class RoomItem : UserControl
    {
        public RoomItem(Room room)
        {
            InitializeComponent();
            this.txtNumber.Text = "Phòng " + room.roomNumber.ToString();
        }
    }
}
