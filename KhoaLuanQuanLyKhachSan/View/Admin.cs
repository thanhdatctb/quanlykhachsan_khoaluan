﻿using KhoaLuanQuanLyKhachSan.Controller;
using KhoaLuanQuanLyKhachSan.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KhoaLuanQuanLyKhachSan.View
{
    public partial class Admin : Form
    {
        private CustomerController customerController = new CustomerController();
        private RoomTypeController roomTypeController = new RoomTypeController();
        public Admin()
        {
            InitializeComponent();
            loadCustomer();
            loadRoomCategory();
            this.BindingRoomTypeCategory();
        }

        private void loadCustomer()
        {
            //this.dtgvCustomer.DataSource = this.customerController.GetAll();

            string sql = "select id, fullName, DiaChi, Phone, IdCard from customers ";
            this.dtgvCustomer.DataSource = MyConnection.ExecuteQuery(sql).DefaultView;
        }
        private void loadRoomCategory()
        {
            this.dtgvRoomCategory.DataSource = this.roomTypeController.GetAll();
        }
        private void BindingRoomTypeCategory()
        {
            this.txtRoomTypeId.DataBindings.Add(new Binding("Text", dtgvRoomCategory.DataSource, "id"));
            this.txtRoomTypeName.DataBindings.Add(new Binding("Text", dtgvRoomCategory.DataSource, "typeName"));
        }
        private void btnAddCustomer_Click(object sender, EventArgs e)
        {
            this.Close();
            new SignUpForm().Show();
        }
        private int getSelectedId()
        {
            int selectedrowindex = dtgvCustomer.SelectedCells[0].RowIndex;
            DataGridViewRow selectedRow = dtgvCustomer.Rows[selectedrowindex];
            return Convert.ToInt32(selectedRow.Cells["id"].Value);
        }
        private void btnViewDetail_Click(object sender, EventArgs e)
        {

            int id = getSelectedId();
            this.Close();
            new CustomerDetail(this.customerController.GetById(id)).Show();
        }

        private void btnDeleteCustomer_Click(object sender, EventArgs e)
        {
            int id = getSelectedId();
            try 
            {
                if (customerController.Delete(id))
                {
                    MessageBox.Show("Xóa khách hàng thành công");
                    loadCustomer();
                }

            }catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                
            }



        }

        private void btnEditCustomer_Click(object sender, EventArgs e)
        {
            int id = getSelectedId();
            this.Close();
            new SignUpForm(this.customerController.GetById(id)).Show();
        }
    }
}
