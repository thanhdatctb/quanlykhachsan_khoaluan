﻿using KhoaLuanQuanLyKhachSan.Controller;
using KhoaLuanQuanLyKhachSan.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KhoaLuanQuanLyKhachSan.View
{
    public partial class CustomerDetail : Form
    {
        private FileController fileController = new FileController();
        public CustomerDetail(Customer customer)
        {
            InitializeComponent();
            this.txtUsername.Text = customer.userName;
            this.txtPassword.Text = customer.hashpass;
            this.txtName.Text = customer.fullName;
            this.txtGender.Text = customer.gender;
            this.txtNationality.Text = customer.customerNational;
            this.dpDateOfBirth.Value = customer.dob;
            this.txtPlaceOfBirth.Text = customer.noiSinh;
            this.txtPhone.Text = customer.Phone;
            this.txtAddress.Text = customer.DiaChi;
            this.txtIdCard.Text = customer.IDCard;
            this.txtPlaceIdCard.Text = customer.IDPlace;
            this.txtPassport.Text = customer.passport;
            this.txtPlacePassport.Text = customer.passportPlace;
            this.pbAva.Image = new Bitmap(fileController.GetImgeLink(customer.img));
            this.txtmail.Text = customer.mail;
        }

        private void label17_Click(object sender, EventArgs e)
        {
            this.Close();
            Admin formAdmin = new Admin();
            formAdmin.tcAdmin.SelectTab("tpCustomer");
            this.Close();
            formAdmin.Show();
        }
    }
}
