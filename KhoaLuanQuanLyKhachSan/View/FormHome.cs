﻿using KhoaLuanQuanLyKhachSan.Controller;
using KhoaLuanQuanLyKhachSan.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KhoaLuanQuanLyKhachSan.View
{
    public partial class FormHome : Form
    {
        private RoomController roomController = new RoomController();
        public FormHome()
        {
            InitializeComponent();
            LoadRoom();
        }
        private void LoadRoom()
        {
            List<Room> rooms = roomController.GetAll();
            foreach(Room room in rooms)
            {

                RoomItem roomItem = new RoomItem(room);
                Button btn = new Button() ;
                btn.Text = room.roomNumber + Environment.NewLine + room.status;
                //btn.Click += btn_Click;
                btn.Tag = room;

                switch (room.status)
                {
                    case true:
                        btn.BackColor = Color.LightSkyBlue;
                        break;
                    default:
                        btn.BackColor = Color.LightSalmon;
                        break;
                }
                flpRooms.Controls.Add(roomItem);
            }    
        }
    }
}
