﻿using KhoaLuanQuanLyKhachSan.Controller;
using KhoaLuanQuanLyKhachSan.Model;
using KhoaLuanQuanLyKhachSan.Session;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KhoaLuanQuanLyKhachSan.View
{
    public partial class SignUpForm : Form
    {
        private Customer inputCustomer = new Customer();
        private string avatarImg = "default.png";
        private FileController fileController = new FileController();
        private CustomerController customerController = new CustomerController();
        public SignUpForm(Customer customer = null) 
        {
            InitializeComponent();
            this.inputCustomer = customer;
            this.cbNationality.DataSource = CountryArrays.Names;
            this.cbNationality.DisplayMember = "CountryArrays.Names";
  
            
            if (customer != null)
            {
                loadData(inputCustomer);
                this.txtConfirm.Enabled = false;
                this.txtPassword.Enabled = false;
                this.btnSignUp.Text = "Cập nhật";
                this.txtTitle.Text = "Cập nhật thông tin khách hàng";
            }
            else
            {
                foreach (String country in CountryArrays.Names)//Đọc tất cả các phần tử trong mảng CountryArrays trong model
                {
                    //count++;
                    //if(country == "Viet Nam") {
                    //    MessageBox.Show(count.ToString());
                    //}

                    //this.cbNationality.Items.Add(country);// Thêm tên các nước vào combobox

                }
                this.cbNationality.SelectedIndex = 231;//đặt giá trị chọn mặc định của combox là giá trị thứ 231(Giá trị này là việt nam)
            }    
            
        }
        private void OpenCustomer()
        {
            Admin formAdmin = new Admin();
            formAdmin.tcAdmin.SelectTab("tpCustomer");
            this.Close();
            formAdmin.Show();
        }
    
        private void loadData(Customer customer)
        {
            this.txtUsername.Text = customer.userName;
            this.txtPassword.Text = customer.hashpass;
            this.txtConfirm.Text = customer.hashpass;
            this.txtName.Text = customer.fullName;
            if(customer.gender != "Nam")
            {
                this.rbFemale.Select();
            }   
            else
            {
                this.rbMale.Select();
            }
            int index = cbNationality.FindString(customer.customerNational);
            this.cbNationality.SelectedIndex = index;
            this.dpDateOfBirth.Value = customer.dob;
            this.txtPlaceOfBirth.Text = customer.noiSinh;
            this.txtPhone.Text = customer.Phone;
            this.txtAddress.Text = customer.DiaChi;
            this.txtIdCard.Text = customer.IDCard;
            this.txtPlaceIdCard.Text = customer.IDPlace;
            this.txtPassport.Text = customer.passport;
            this.txtPlacePassport.Text = customer.passportPlace;
            this.pbAva.Image = new Bitmap(fileController.GetImgeLink(customer.img));
            this.avatarImg = fileController.GetImgeLink(customer.img);
            this.txtEmail.Text = customer.mail;
        }
        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void label17_Click(object sender, EventArgs e)
        {
            if(MySession.admin != null)
            {
                OpenCustomer();
                return;
            }    
            Application.Exit();
        }

        private void button2_Click(object sender, EventArgs e)//Hàm này sẽ thực thi khi ấn nút chọn ảnh
        {
            OpenFileDialog opnfd = new OpenFileDialog();//tạo ra 1 cửa sổ chọn file
            opnfd.Filter = "Image Files (*.jpg;*.jpeg;.*.gif;)|*.jpg;*.jpeg;.*.gif";//Lọc và hiển thị các file ảnh có đuôi .jpg, .jpeg,...
            opnfd.ShowDialog();//mở cửa sổ chọn file vừa tạo
            this.avatarImg = opnfd.FileName;//gán biến avatarImg bằng tên file người dùng cọn
            this.pbAva.Image = new Bitmap(avatarImg);//chuyển ảnh hiển thị sang ảnh người dùng chọn

        }

        private void button1_Click(object sender, EventArgs e)//Hàm này thực thi khi ấn nút đăng ký
        {
            String userName = this.txtUsername.Text;
            String hashpass = PasswordController.Sha1Hash(this.txtPassword.Text);
            String hashconfirm = PasswordController.Sha1Hash(this.txtConfirm.Text);
            String email = this.txtEmail.Text;
            String name = this.txtName.Text;
            string address = this.txtAddress.Text;
            //Nam ghi 0, nu ghi 1
            string gender = "Nam";
            if (rbFemale.Checked)
            {
                gender = "Nữ";
            }
            if(hashconfirm != hashpass)
            {
                MessageBox.Show("Mật khẩu không khớp");
                return;
            }    
            String nationality = this.cbNationality.SelectedItem.ToString();//Lấy giá trị quốc tịch trong combobox
            DateTime dob = this.dpDateOfBirth.Value;
            string PlaceOfBirth = this.txtPlaceOfBirth.Text;
            string phone = this.txtPhone.Text;
            string IdCard = this.txtIdCard.Text;
            string PlaceIdCard = this.txtPlaceIdCard.Text;
            string passport = this.txtPassport.Text;
            string PlacePassport =  this.txtPlacePassport.Text;
            String avatarImgName;
            if (this.avatarImg != "default.png")//Nếu avaImg không phải là ảnh mặc định
            {
                this.fileController.MoveFileToImageFolder(avatarImg);//Copy ảnh vào Folder resource
                avatarImgName = this.avatarImg.Split('\\').Last();//Lấy tên ảnh đó gán vào biến avatarImgName
            }
            else
            {
                avatarImgName = avatarImg;//đặt avatarImgName bằng giá trị mặc định
            }
            
            Customer customer = new Customer(userName,hashpass,name,gender,nationality,dob,PlaceOfBirth,address,phone,email,IdCard,PlaceIdCard,passport,PlacePassport);
            customer.img = avatarImgName;
            try 
            {
                if(inputCustomer == null)
                {
                    if (customerController.Add(customer))
                    {
                        MessageBox.Show("Đăng kí khách hàng mới thành công");
                        this.Close();
                        if (MySession.admin != null)
                        { 
                            OpenCustomer();
                        }else
                        {
                            new View.FormHome().Show();
                        }    
                    }
                }   
                else
                {
                    this.txtId.Text = inputCustomer.id.ToString();
                    customer.id = inputCustomer.id;
                    if (customerController.Edit(customer))
                    {
                        MessageBox.Show("Sửa thông tin khách hàng thành công");
                        this.Close();
                        if (MySession.admin != null)
                        {
                            OpenCustomer();   
                        }
                    }
                }    
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
           
        }

        private void SignUpForm_Load(object sender, EventArgs e)//Hàm này sẽ chạy khi Form đăng kí được gọi
        {
            int count = 0;
            
        }
    }
}
