﻿using KhoaLuanQuanLyKhachSan.Controller;
using KhoaLuanQuanLyKhachSan.Model;
using KhoaLuanQuanLyKhachSan.Session;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KhoaLuanQuanLyKhachSan.View
{
    public partial class LoginForm : Form
    {
        public LoginForm()
        {
            InitializeComponent();
        }

        private void label5_Click(object sender, EventArgs e)//Hàm này thực thi khi ấn vào chữ Đăng ký
        {
            this.Hide();//Cửa sổ hiện tại sẽ ẩn đi
            new SignUpForm().Show();//Cửa sổ SignUpform sẽ hiện lên
        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void btnLogIn_Click(object sender, EventArgs e)//Hàm này thực thi khi ấn nút Đăng nhập
        {
            string userName = this.txtUsername.Text;//Lấy giá trị của textbox tên txtUsername gán vào biến userName
            string password = this.txtPassword.Text; // Lấy giá trị của textbox tên txtPassword gán vào biến password
            AdminController adminController = new Controller.AdminController();//Tạo ra 1 biến adminController 
            CustomerController customerController = new CustomerController();//Tạo ra 1 biến customerController
            if(adminController.LogIn(userName,password))//Nếu login bằng tài khoản admin hợp lệ
            {
                new Admin().Show();
            }else if(customerController.LogIn(userName,password))//Nếu login bằng tài khoản customer hợp lệ
            {
                //MessageBox.Show("Hello " + MySession.customer.fullName);
                new FormHome().Show();
            }
            else
            {
                MessageBox.Show("Tên đăng nhập hoặc mật khẩu không đúng");
            }
            
        }
    }
}
