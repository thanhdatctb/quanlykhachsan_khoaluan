﻿namespace KhoaLuanQuanLyKhachSan.View
{
    partial class Admin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tcAdmin = new System.Windows.Forms.TabControl();
            this.tpBill = new System.Windows.Forms.TabPage();
            this.panel2 = new System.Windows.Forms.Panel();
            this.cbExportExcel = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.btnExport = new System.Windows.Forms.Button();
            this.btnViewBill1 = new System.Windows.Forms.Button();
            this.dtpkToDate1 = new System.Windows.Forms.DateTimePicker();
            this.dtpkFromDate1 = new System.Windows.Forms.DateTimePicker();
            this.panel1 = new System.Windows.Forms.Panel();
            this.dtgvBill1 = new System.Windows.Forms.DataGridView();
            this.tpRoomCategory = new System.Windows.Forms.TabPage();
            this.txtCategoryName = new System.Windows.Forms.TextBox();
            this.txtCategoryID = new System.Windows.Forms.TextBox();
            this.dtgvRoomCategory = new System.Windows.Forms.DataGridView();
            this.panel11 = new System.Windows.Forms.Panel();
            this.btnEditMenu = new System.Windows.Forms.Button();
            this.btnAddMenu = new System.Windows.Forms.Button();
            this.tpFood = new System.Windows.Forms.TabPage();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel10 = new System.Windows.Forms.Panel();
            this.nmFoodPrice = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.panel9 = new System.Windows.Forms.Panel();
            this.cbCategory = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.panel8 = new System.Windows.Forms.Panel();
            this.txbFoodName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.txbFoodID = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btnEditFood = new System.Windows.Forms.Button();
            this.btnDeleteFood = new System.Windows.Forms.Button();
            this.btnAddFood = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.dtgvFood = new System.Windows.Forms.DataGridView();
            this.tpCustomer = new System.Windows.Forms.TabPage();
            this.panel22 = new System.Windows.Forms.Panel();
            this.panel24 = new System.Windows.Forms.Panel();
            this.cbType = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.panel25 = new System.Windows.Forms.Panel();
            this.txbDisplayName = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.panel26 = new System.Windows.Forms.Panel();
            this.txbUserName = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.panel27 = new System.Windows.Forms.Panel();
            this.btnViewDetail = new System.Windows.Forms.Button();
            this.btnEditCustomer = new System.Windows.Forms.Button();
            this.btnDeleteCustomer = new System.Windows.Forms.Button();
            this.btnAddCustomer = new System.Windows.Forms.Button();
            this.panel28 = new System.Windows.Forms.Panel();
            this.dtgvCustomer = new System.Windows.Forms.DataGridView();
            this.tpTable = new System.Windows.Forms.TabPage();
            this.panel6 = new System.Windows.Forms.Panel();
            this.BtnEditTable = new System.Windows.Forms.Button();
            this.BtnDeleteTable = new System.Windows.Forms.Button();
            this.btnAddTable = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cbTableStatus = new System.Windows.Forms.ComboBox();
            this.txtTableName = new System.Windows.Forms.TextBox();
            this.txtTableId = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.dtgvTable = new System.Windows.Forms.DataGridView();
            this.tpRole = new System.Windows.Forms.TabPage();
            this.cbStaff = new System.Windows.Forms.ComboBox();
            this.cbAdmin = new System.Windows.Forms.ComboBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.panel12 = new System.Windows.Forms.Panel();
            this.txtRoomTypeId = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtRoomTypeName = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.tcAdmin.SuspendLayout();
            this.tpBill.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgvBill1)).BeginInit();
            this.tpRoomCategory.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgvRoomCategory)).BeginInit();
            this.panel11.SuspendLayout();
            this.tpFood.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nmFoodPrice)).BeginInit();
            this.panel9.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgvFood)).BeginInit();
            this.tpCustomer.SuspendLayout();
            this.panel22.SuspendLayout();
            this.panel24.SuspendLayout();
            this.panel25.SuspendLayout();
            this.panel26.SuspendLayout();
            this.panel27.SuspendLayout();
            this.panel28.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgvCustomer)).BeginInit();
            this.tpTable.SuspendLayout();
            this.panel6.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgvTable)).BeginInit();
            this.tpRole.SuspendLayout();
            this.panel12.SuspendLayout();
            this.SuspendLayout();
            // 
            // tcAdmin
            // 
            this.tcAdmin.Controls.Add(this.tpBill);
            this.tcAdmin.Controls.Add(this.tpRoomCategory);
            this.tcAdmin.Controls.Add(this.tpFood);
            this.tcAdmin.Controls.Add(this.tpCustomer);
            this.tcAdmin.Controls.Add(this.tpTable);
            this.tcAdmin.Controls.Add(this.tpRole);
            this.tcAdmin.Location = new System.Drawing.Point(3, 3);
            this.tcAdmin.Name = "tcAdmin";
            this.tcAdmin.SelectedIndex = 0;
            this.tcAdmin.Size = new System.Drawing.Size(746, 472);
            this.tcAdmin.TabIndex = 1;
            // 
            // tpBill
            // 
            this.tpBill.Controls.Add(this.panel2);
            this.tpBill.Controls.Add(this.panel1);
            this.tpBill.Location = new System.Drawing.Point(4, 22);
            this.tpBill.Name = "tpBill";
            this.tpBill.Padding = new System.Windows.Forms.Padding(3);
            this.tpBill.Size = new System.Drawing.Size(738, 446);
            this.tpBill.TabIndex = 0;
            this.tpBill.Text = "Doanh thu";
            this.tpBill.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.cbExportExcel);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.label15);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.btnExport);
            this.panel2.Controls.Add(this.btnViewBill1);
            this.panel2.Controls.Add(this.dtpkToDate1);
            this.panel2.Controls.Add(this.dtpkFromDate1);
            this.panel2.Location = new System.Drawing.Point(6, 6);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(723, 97);
            this.panel2.TabIndex = 1;
            // 
            // cbExportExcel
            // 
            this.cbExportExcel.FormattingEnabled = true;
            this.cbExportExcel.Items.AddRange(new object[] {
            "Tổng doanh thu theo ngày",
            "Tổng doanh thu theo tháng"});
            this.cbExportExcel.Location = new System.Drawing.Point(78, 54);
            this.cbExportExcel.Name = "cbExportExcel";
            this.cbExportExcel.Size = new System.Drawing.Size(172, 21);
            this.cbExportExcel.TabIndex = 12;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(276, 15);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(35, 16);
            this.label6.TabIndex = 11;
            this.label6.Text = "Đến:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(9, 55);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(63, 16);
            this.label15.TabIndex = 10;
            this.label15.Text = "Xuất theo";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(9, 12);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(27, 16);
            this.label5.TabIndex = 10;
            this.label5.Text = "Từ:";
            // 
            // btnExport
            // 
            this.btnExport.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btnExport.Location = new System.Drawing.Point(546, 45);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(79, 37);
            this.btnExport.TabIndex = 2;
            this.btnExport.Text = "Xuất Excel";
            this.btnExport.UseVisualStyleBackColor = false;
            // 
            // btnViewBill1
            // 
            this.btnViewBill1.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btnViewBill1.Location = new System.Drawing.Point(546, 2);
            this.btnViewBill1.Name = "btnViewBill1";
            this.btnViewBill1.Size = new System.Drawing.Size(79, 37);
            this.btnViewBill1.TabIndex = 2;
            this.btnViewBill1.Text = "Thống kê";
            this.btnViewBill1.UseVisualStyleBackColor = false;
            // 
            // dtpkToDate1
            // 
            this.dtpkToDate1.Location = new System.Drawing.Point(317, 12);
            this.dtpkToDate1.Name = "dtpkToDate1";
            this.dtpkToDate1.Size = new System.Drawing.Size(208, 20);
            this.dtpkToDate1.TabIndex = 1;
            // 
            // dtpkFromDate1
            // 
            this.dtpkFromDate1.Location = new System.Drawing.Point(42, 12);
            this.dtpkFromDate1.Name = "dtpkFromDate1";
            this.dtpkFromDate1.Size = new System.Drawing.Size(208, 20);
            this.dtpkFromDate1.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.dtgvBill1);
            this.panel1.Location = new System.Drawing.Point(6, 109);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(726, 273);
            this.panel1.TabIndex = 0;
            // 
            // dtgvBill1
            // 
            this.dtgvBill1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dtgvBill1.BackgroundColor = System.Drawing.Color.MistyRose;
            this.dtgvBill1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgvBill1.Location = new System.Drawing.Point(6, 3);
            this.dtgvBill1.Name = "dtgvBill1";
            this.dtgvBill1.Size = new System.Drawing.Size(723, 270);
            this.dtgvBill1.TabIndex = 0;
            // 
            // tpRoomCategory
            // 
            this.tpRoomCategory.Controls.Add(this.label20);
            this.tpRoomCategory.Controls.Add(this.panel12);
            this.tpRoomCategory.Controls.Add(this.txtCategoryName);
            this.tpRoomCategory.Controls.Add(this.txtCategoryID);
            this.tpRoomCategory.Controls.Add(this.dtgvRoomCategory);
            this.tpRoomCategory.Controls.Add(this.panel11);
            this.tpRoomCategory.ForeColor = System.Drawing.SystemColors.InfoText;
            this.tpRoomCategory.Location = new System.Drawing.Point(4, 22);
            this.tpRoomCategory.Name = "tpRoomCategory";
            this.tpRoomCategory.Padding = new System.Windows.Forms.Padding(3);
            this.tpRoomCategory.Size = new System.Drawing.Size(738, 446);
            this.tpRoomCategory.TabIndex = 6;
            this.tpRoomCategory.Text = "Quản lý danh mục phòng";
            this.tpRoomCategory.UseVisualStyleBackColor = true;
            // 
            // txtCategoryName
            // 
            this.txtCategoryName.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtCategoryName.Enabled = false;
            this.txtCategoryName.ForeColor = System.Drawing.SystemColors.Window;
            this.txtCategoryName.Location = new System.Drawing.Point(628, 182);
            this.txtCategoryName.Name = "txtCategoryName";
            this.txtCategoryName.Size = new System.Drawing.Size(0, 13);
            this.txtCategoryName.TabIndex = 4;
            // 
            // txtCategoryID
            // 
            this.txtCategoryID.BackColor = System.Drawing.SystemColors.Window;
            this.txtCategoryID.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtCategoryID.Enabled = false;
            this.txtCategoryID.ForeColor = System.Drawing.SystemColors.Window;
            this.txtCategoryID.Location = new System.Drawing.Point(628, 130);
            this.txtCategoryID.Name = "txtCategoryID";
            this.txtCategoryID.Size = new System.Drawing.Size(0, 13);
            this.txtCategoryID.TabIndex = 4;
            // 
            // dtgvRoomCategory
            // 
            this.dtgvRoomCategory.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dtgvRoomCategory.BackgroundColor = System.Drawing.Color.MistyRose;
            this.dtgvRoomCategory.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgvRoomCategory.GridColor = System.Drawing.Color.RosyBrown;
            this.dtgvRoomCategory.Location = new System.Drawing.Point(63, 222);
            this.dtgvRoomCategory.Name = "dtgvRoomCategory";
            this.dtgvRoomCategory.Size = new System.Drawing.Size(611, 180);
            this.dtgvRoomCategory.TabIndex = 3;
            // 
            // panel11
            // 
            this.panel11.Controls.Add(this.btnEditMenu);
            this.panel11.Controls.Add(this.btnAddMenu);
            this.panel11.Location = new System.Drawing.Point(535, 67);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(139, 138);
            this.panel11.TabIndex = 2;
            // 
            // btnEditMenu
            // 
            this.btnEditMenu.BackColor = System.Drawing.Color.Moccasin;
            this.btnEditMenu.Location = new System.Drawing.Point(34, 90);
            this.btnEditMenu.Name = "btnEditMenu";
            this.btnEditMenu.Size = new System.Drawing.Size(75, 40);
            this.btnEditMenu.TabIndex = 2;
            this.btnEditMenu.Text = "Sửa";
            this.btnEditMenu.UseVisualStyleBackColor = false;
            // 
            // btnAddMenu
            // 
            this.btnAddMenu.BackColor = System.Drawing.Color.Moccasin;
            this.btnAddMenu.Location = new System.Drawing.Point(34, 0);
            this.btnAddMenu.Name = "btnAddMenu";
            this.btnAddMenu.Size = new System.Drawing.Size(75, 40);
            this.btnAddMenu.TabIndex = 0;
            this.btnAddMenu.Text = "Thêm";
            this.btnAddMenu.UseVisualStyleBackColor = false;
            // 
            // tpFood
            // 
            this.tpFood.Controls.Add(this.panel5);
            this.tpFood.Controls.Add(this.panel4);
            this.tpFood.Controls.Add(this.panel3);
            this.tpFood.Location = new System.Drawing.Point(4, 22);
            this.tpFood.Name = "tpFood";
            this.tpFood.Padding = new System.Windows.Forms.Padding(3);
            this.tpFood.Size = new System.Drawing.Size(738, 446);
            this.tpFood.TabIndex = 1;
            this.tpFood.Text = "Thực đơn";
            this.tpFood.UseVisualStyleBackColor = true;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.panel10);
            this.panel5.Controls.Add(this.panel9);
            this.panel5.Controls.Add(this.panel8);
            this.panel5.Controls.Add(this.panel7);
            this.panel5.Location = new System.Drawing.Point(690, 39);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(0, 10);
            this.panel5.TabIndex = 2;
            // 
            // panel10
            // 
            this.panel10.Controls.Add(this.nmFoodPrice);
            this.panel10.Controls.Add(this.label4);
            this.panel10.Location = new System.Drawing.Point(6, 205);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(323, 46);
            this.panel10.TabIndex = 4;
            // 
            // nmFoodPrice
            // 
            this.nmFoodPrice.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.nmFoodPrice.Location = new System.Drawing.Point(115, 13);
            this.nmFoodPrice.Maximum = new decimal(new int[] {
            100000000,
            0,
            0,
            0});
            this.nmFoodPrice.Name = "nmFoodPrice";
            this.nmFoodPrice.Size = new System.Drawing.Size(204, 20);
            this.nmFoodPrice.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(16, 13);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(40, 19);
            this.label4.TabIndex = 0;
            this.label4.Text = "Giá:";
            // 
            // panel9
            // 
            this.panel9.Controls.Add(this.cbCategory);
            this.panel9.Controls.Add(this.label3);
            this.panel9.Location = new System.Drawing.Point(6, 142);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(323, 46);
            this.panel9.TabIndex = 3;
            // 
            // cbCategory
            // 
            this.cbCategory.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.cbCategory.FormattingEnabled = true;
            this.cbCategory.Location = new System.Drawing.Point(115, 14);
            this.cbCategory.Name = "cbCategory";
            this.cbCategory.Size = new System.Drawing.Size(204, 21);
            this.cbCategory.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(16, 13);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(93, 19);
            this.label3.TabIndex = 0;
            this.label3.Text = "Danh mục:";
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.txbFoodName);
            this.panel8.Controls.Add(this.label2);
            this.panel8.Location = new System.Drawing.Point(6, 80);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(323, 46);
            this.panel8.TabIndex = 2;
            // 
            // txbFoodName
            // 
            this.txbFoodName.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.txbFoodName.Location = new System.Drawing.Point(115, 12);
            this.txbFoodName.Name = "txbFoodName";
            this.txbFoodName.Size = new System.Drawing.Size(204, 20);
            this.txbFoodName.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(16, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 19);
            this.label2.TabIndex = 0;
            this.label2.Text = "Tên món:";
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.txbFoodID);
            this.panel7.Controls.Add(this.label1);
            this.panel7.Location = new System.Drawing.Point(6, 17);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(323, 46);
            this.panel7.TabIndex = 1;
            // 
            // txbFoodID
            // 
            this.txbFoodID.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.txbFoodID.Location = new System.Drawing.Point(115, 12);
            this.txbFoodID.Name = "txbFoodID";
            this.txbFoodID.Size = new System.Drawing.Size(204, 20);
            this.txbFoodID.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(16, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(31, 19);
            this.label1.TabIndex = 0;
            this.label1.Text = "ID:";
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.btnEditFood);
            this.panel4.Controls.Add(this.btnDeleteFood);
            this.panel4.Controls.Add(this.btnAddFood);
            this.panel4.Location = new System.Drawing.Point(40, 6);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(606, 46);
            this.panel4.TabIndex = 1;
            // 
            // btnEditFood
            // 
            this.btnEditFood.BackColor = System.Drawing.Color.Moccasin;
            this.btnEditFood.Location = new System.Drawing.Point(491, 3);
            this.btnEditFood.Name = "btnEditFood";
            this.btnEditFood.Size = new System.Drawing.Size(75, 40);
            this.btnEditFood.TabIndex = 2;
            this.btnEditFood.Text = "Sửa";
            this.btnEditFood.UseVisualStyleBackColor = false;
            // 
            // btnDeleteFood
            // 
            this.btnDeleteFood.BackColor = System.Drawing.Color.Moccasin;
            this.btnDeleteFood.Location = new System.Drawing.Point(274, 3);
            this.btnDeleteFood.Name = "btnDeleteFood";
            this.btnDeleteFood.Size = new System.Drawing.Size(75, 40);
            this.btnDeleteFood.TabIndex = 1;
            this.btnDeleteFood.Text = "Xóa";
            this.btnDeleteFood.UseVisualStyleBackColor = false;
            // 
            // btnAddFood
            // 
            this.btnAddFood.BackColor = System.Drawing.Color.Moccasin;
            this.btnAddFood.Location = new System.Drawing.Point(62, 3);
            this.btnAddFood.Name = "btnAddFood";
            this.btnAddFood.Size = new System.Drawing.Size(75, 40);
            this.btnAddFood.TabIndex = 0;
            this.btnAddFood.Text = "Thêm";
            this.btnAddFood.UseVisualStyleBackColor = false;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.dtgvFood);
            this.panel3.Location = new System.Drawing.Point(6, 58);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(640, 361);
            this.panel3.TabIndex = 0;
            // 
            // dtgvFood
            // 
            this.dtgvFood.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dtgvFood.BackgroundColor = System.Drawing.Color.MistyRose;
            this.dtgvFood.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgvFood.GridColor = System.Drawing.Color.RosyBrown;
            this.dtgvFood.Location = new System.Drawing.Point(34, 10);
            this.dtgvFood.Name = "dtgvFood";
            this.dtgvFood.Size = new System.Drawing.Size(583, 318);
            this.dtgvFood.TabIndex = 0;
            // 
            // tpCustomer
            // 
            this.tpCustomer.Controls.Add(this.panel22);
            this.tpCustomer.Controls.Add(this.panel27);
            this.tpCustomer.Controls.Add(this.panel28);
            this.tpCustomer.Location = new System.Drawing.Point(4, 22);
            this.tpCustomer.Name = "tpCustomer";
            this.tpCustomer.Padding = new System.Windows.Forms.Padding(3);
            this.tpCustomer.Size = new System.Drawing.Size(738, 446);
            this.tpCustomer.TabIndex = 4;
            this.tpCustomer.Text = "Khách hàng";
            this.tpCustomer.UseVisualStyleBackColor = true;
            // 
            // panel22
            // 
            this.panel22.Controls.Add(this.panel24);
            this.panel22.Controls.Add(this.panel25);
            this.panel22.Controls.Add(this.panel26);
            this.panel22.Location = new System.Drawing.Point(722, 58);
            this.panel22.Name = "panel22";
            this.panel22.Size = new System.Drawing.Size(10, 324);
            this.panel22.TabIndex = 5;
            // 
            // panel24
            // 
            this.panel24.Controls.Add(this.cbType);
            this.panel24.Controls.Add(this.label11);
            this.panel24.Location = new System.Drawing.Point(6, 120);
            this.panel24.Name = "panel24";
            this.panel24.Size = new System.Drawing.Size(311, 46);
            this.panel24.TabIndex = 3;
            // 
            // cbType
            // 
            this.cbType.FormattingEnabled = true;
            this.cbType.Items.AddRange(new object[] {
            "Staff",
            "Admin"});
            this.cbType.Location = new System.Drawing.Point(127, 11);
            this.cbType.Name = "cbType";
            this.cbType.Size = new System.Drawing.Size(181, 21);
            this.cbType.TabIndex = 2;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(6, 13);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(122, 19);
            this.label11.TabIndex = 0;
            this.label11.Text = "Loại tài khoản:";
            // 
            // panel25
            // 
            this.panel25.Controls.Add(this.txbDisplayName);
            this.panel25.Controls.Add(this.label12);
            this.panel25.Location = new System.Drawing.Point(6, 68);
            this.panel25.Name = "panel25";
            this.panel25.Size = new System.Drawing.Size(311, 46);
            this.panel25.TabIndex = 2;
            // 
            // txbDisplayName
            // 
            this.txbDisplayName.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.txbDisplayName.Location = new System.Drawing.Point(127, 12);
            this.txbDisplayName.Name = "txbDisplayName";
            this.txbDisplayName.Size = new System.Drawing.Size(181, 20);
            this.txbDisplayName.TabIndex = 1;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(5, 11);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(104, 19);
            this.label12.TabIndex = 0;
            this.label12.Text = "Tên hiển thị:";
            // 
            // panel26
            // 
            this.panel26.Controls.Add(this.txbUserName);
            this.panel26.Controls.Add(this.label13);
            this.panel26.Location = new System.Drawing.Point(6, 16);
            this.panel26.Name = "panel26";
            this.panel26.Size = new System.Drawing.Size(311, 46);
            this.panel26.TabIndex = 1;
            // 
            // txbUserName
            // 
            this.txbUserName.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.txbUserName.Location = new System.Drawing.Point(127, 12);
            this.txbUserName.Name = "txbUserName";
            this.txbUserName.Size = new System.Drawing.Size(181, 20);
            this.txbUserName.TabIndex = 1;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(3, 13);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(118, 19);
            this.label13.TabIndex = 0;
            this.label13.Text = "Tên tài khoản:";
            // 
            // panel27
            // 
            this.panel27.Controls.Add(this.btnViewDetail);
            this.panel27.Controls.Add(this.btnEditCustomer);
            this.panel27.Controls.Add(this.btnDeleteCustomer);
            this.panel27.Controls.Add(this.btnAddCustomer);
            this.panel27.Location = new System.Drawing.Point(611, 17);
            this.panel27.Name = "panel27";
            this.panel27.Size = new System.Drawing.Size(105, 313);
            this.panel27.TabIndex = 4;
            // 
            // btnViewDetail
            // 
            this.btnViewDetail.BackColor = System.Drawing.Color.Moccasin;
            this.btnViewDetail.Location = new System.Drawing.Point(14, 203);
            this.btnViewDetail.Name = "btnViewDetail";
            this.btnViewDetail.Size = new System.Drawing.Size(75, 40);
            this.btnViewDetail.TabIndex = 2;
            this.btnViewDetail.Text = "Xem chi tiết";
            this.btnViewDetail.UseVisualStyleBackColor = false;
            this.btnViewDetail.Click += new System.EventHandler(this.btnViewDetail_Click);
            // 
            // btnEditCustomer
            // 
            this.btnEditCustomer.BackColor = System.Drawing.Color.Moccasin;
            this.btnEditCustomer.Location = new System.Drawing.Point(14, 144);
            this.btnEditCustomer.Name = "btnEditCustomer";
            this.btnEditCustomer.Size = new System.Drawing.Size(75, 40);
            this.btnEditCustomer.TabIndex = 2;
            this.btnEditCustomer.Text = "Sửa thông tin";
            this.btnEditCustomer.UseVisualStyleBackColor = false;
            this.btnEditCustomer.Click += new System.EventHandler(this.btnEditCustomer_Click);
            // 
            // btnDeleteCustomer
            // 
            this.btnDeleteCustomer.BackColor = System.Drawing.Color.Moccasin;
            this.btnDeleteCustomer.Location = new System.Drawing.Point(14, 88);
            this.btnDeleteCustomer.Name = "btnDeleteCustomer";
            this.btnDeleteCustomer.Size = new System.Drawing.Size(75, 40);
            this.btnDeleteCustomer.TabIndex = 1;
            this.btnDeleteCustomer.Text = "Xóa";
            this.btnDeleteCustomer.UseVisualStyleBackColor = false;
            this.btnDeleteCustomer.Click += new System.EventHandler(this.btnDeleteCustomer_Click);
            // 
            // btnAddCustomer
            // 
            this.btnAddCustomer.BackColor = System.Drawing.Color.Moccasin;
            this.btnAddCustomer.Location = new System.Drawing.Point(14, 26);
            this.btnAddCustomer.Name = "btnAddCustomer";
            this.btnAddCustomer.Size = new System.Drawing.Size(75, 40);
            this.btnAddCustomer.TabIndex = 0;
            this.btnAddCustomer.Text = "Khách hàng mới";
            this.btnAddCustomer.UseVisualStyleBackColor = false;
            this.btnAddCustomer.Click += new System.EventHandler(this.btnAddCustomer_Click);
            // 
            // panel28
            // 
            this.panel28.Controls.Add(this.label19);
            this.panel28.Controls.Add(this.dtgvCustomer);
            this.panel28.Location = new System.Drawing.Point(12, 6);
            this.panel28.Name = "panel28";
            this.panel28.Size = new System.Drawing.Size(593, 324);
            this.panel28.TabIndex = 3;
            // 
            // dtgvCustomer
            // 
            this.dtgvCustomer.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dtgvCustomer.BackgroundColor = System.Drawing.Color.MistyRose;
            this.dtgvCustomer.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgvCustomer.Location = new System.Drawing.Point(3, 80);
            this.dtgvCustomer.Name = "dtgvCustomer";
            this.dtgvCustomer.Size = new System.Drawing.Size(577, 241);
            this.dtgvCustomer.TabIndex = 0;
            // 
            // tpTable
            // 
            this.tpTable.Controls.Add(this.panel6);
            this.tpTable.Controls.Add(this.groupBox1);
            this.tpTable.Controls.Add(this.dtgvTable);
            this.tpTable.Location = new System.Drawing.Point(4, 22);
            this.tpTable.Name = "tpTable";
            this.tpTable.Padding = new System.Windows.Forms.Padding(3);
            this.tpTable.Size = new System.Drawing.Size(738, 446);
            this.tpTable.TabIndex = 5;
            this.tpTable.Text = "Bàn ăn";
            this.tpTable.UseVisualStyleBackColor = true;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.BtnEditTable);
            this.panel6.Controls.Add(this.BtnDeleteTable);
            this.panel6.Controls.Add(this.btnAddTable);
            this.panel6.Location = new System.Drawing.Point(6, 12);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(649, 46);
            this.panel6.TabIndex = 5;
            // 
            // BtnEditTable
            // 
            this.BtnEditTable.BackColor = System.Drawing.Color.Moccasin;
            this.BtnEditTable.Location = new System.Drawing.Point(281, 3);
            this.BtnEditTable.Name = "BtnEditTable";
            this.BtnEditTable.Size = new System.Drawing.Size(75, 40);
            this.BtnEditTable.TabIndex = 2;
            this.BtnEditTable.Text = "Sửa";
            this.BtnEditTable.UseVisualStyleBackColor = false;
            // 
            // BtnDeleteTable
            // 
            this.BtnDeleteTable.BackColor = System.Drawing.Color.Moccasin;
            this.BtnDeleteTable.Location = new System.Drawing.Point(513, 3);
            this.BtnDeleteTable.Name = "BtnDeleteTable";
            this.BtnDeleteTable.Size = new System.Drawing.Size(75, 40);
            this.BtnDeleteTable.TabIndex = 1;
            this.BtnDeleteTable.Text = "Xóa";
            this.BtnDeleteTable.UseVisualStyleBackColor = false;
            // 
            // btnAddTable
            // 
            this.btnAddTable.BackColor = System.Drawing.Color.Moccasin;
            this.btnAddTable.Location = new System.Drawing.Point(56, 3);
            this.btnAddTable.Name = "btnAddTable";
            this.btnAddTable.Size = new System.Drawing.Size(75, 40);
            this.btnAddTable.TabIndex = 0;
            this.btnAddTable.Text = "Thêm";
            this.btnAddTable.UseVisualStyleBackColor = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cbTableStatus);
            this.groupBox1.Controls.Add(this.txtTableName);
            this.groupBox1.Controls.Add(this.txtTableId);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Location = new System.Drawing.Point(402, 64);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(330, 0);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            // 
            // cbTableStatus
            // 
            this.cbTableStatus.FormattingEnabled = true;
            this.cbTableStatus.Items.AddRange(new object[] {
            "Trống",
            "Có người"});
            this.cbTableStatus.Location = new System.Drawing.Point(105, 151);
            this.cbTableStatus.Name = "cbTableStatus";
            this.cbTableStatus.Size = new System.Drawing.Size(179, 21);
            this.cbTableStatus.TabIndex = 2;
            // 
            // txtTableName
            // 
            this.txtTableName.Location = new System.Drawing.Point(105, 110);
            this.txtTableName.Name = "txtTableName";
            this.txtTableName.Size = new System.Drawing.Size(179, 20);
            this.txtTableName.TabIndex = 1;
            // 
            // txtTableId
            // 
            this.txtTableId.Enabled = false;
            this.txtTableId.Location = new System.Drawing.Point(105, 67);
            this.txtTableId.Name = "txtTableId";
            this.txtTableId.Size = new System.Drawing.Size(179, 20);
            this.txtTableId.TabIndex = 1;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.label14.Location = new System.Drawing.Point(19, 154);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(85, 19);
            this.label14.TabIndex = 0;
            this.label14.Text = "Trạng thái";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.label10.Location = new System.Drawing.Point(19, 109);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(71, 19);
            this.label10.TabIndex = 0;
            this.label10.Text = "Tên bàn";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.label9.Location = new System.Drawing.Point(19, 68);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(25, 19);
            this.label9.TabIndex = 0;
            this.label9.Text = "ID";
            // 
            // dtgvTable
            // 
            this.dtgvTable.BackgroundColor = System.Drawing.Color.MistyRose;
            this.dtgvTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgvTable.Location = new System.Drawing.Point(6, 64);
            this.dtgvTable.Name = "dtgvTable";
            this.dtgvTable.Size = new System.Drawing.Size(649, 303);
            this.dtgvTable.TabIndex = 0;
            // 
            // tpRole
            // 
            this.tpRole.Controls.Add(this.cbStaff);
            this.tpRole.Controls.Add(this.cbAdmin);
            this.tpRole.Controls.Add(this.label18);
            this.tpRole.Controls.Add(this.label17);
            this.tpRole.Controls.Add(this.label16);
            this.tpRole.Location = new System.Drawing.Point(4, 22);
            this.tpRole.Name = "tpRole";
            this.tpRole.Padding = new System.Windows.Forms.Padding(3);
            this.tpRole.Size = new System.Drawing.Size(738, 446);
            this.tpRole.TabIndex = 7;
            this.tpRole.Text = "Phân quyền";
            this.tpRole.UseVisualStyleBackColor = true;
            // 
            // cbStaff
            // 
            this.cbStaff.FormattingEnabled = true;
            this.cbStaff.Items.AddRange(new object[] {
            "Bán hàng",
            "Cập nhật thông tin cá nhân"});
            this.cbStaff.Location = new System.Drawing.Point(196, 151);
            this.cbStaff.Name = "cbStaff";
            this.cbStaff.Size = new System.Drawing.Size(160, 21);
            this.cbStaff.TabIndex = 3;
            // 
            // cbAdmin
            // 
            this.cbAdmin.FormattingEnabled = true;
            this.cbAdmin.Items.AddRange(new object[] {
            "Bán hàng",
            "Quản lý tài khoản",
            "Quản lý danh mục thực đơn",
            "Quản lý bàn",
            "Báo cáo"});
            this.cbAdmin.Location = new System.Drawing.Point(196, 103);
            this.cbAdmin.Name = "cbAdmin";
            this.cbAdmin.Size = new System.Drawing.Size(160, 21);
            this.cbAdmin.TabIndex = 2;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(84, 154);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(29, 13);
            this.label18.TabIndex = 1;
            this.label18.Text = "Staff";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(84, 106);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(36, 13);
            this.label17.TabIndex = 1;
            this.label17.Text = "Admin";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(286, 34);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(93, 20);
            this.label16.TabIndex = 0;
            this.label16.Text = "Phân quyền";
            // 
            // panel12
            // 
            this.panel12.Controls.Add(this.txtRoomTypeId);
            this.panel12.Controls.Add(this.label7);
            this.panel12.Controls.Add(this.txtRoomTypeName);
            this.panel12.Controls.Add(this.label8);
            this.panel12.Location = new System.Drawing.Point(63, 67);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(437, 138);
            this.panel12.TabIndex = 5;
            // 
            // txtRoomTypeId
            // 
            this.txtRoomTypeId.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.txtRoomTypeId.Location = new System.Drawing.Point(159, 20);
            this.txtRoomTypeId.Name = "txtRoomTypeId";
            this.txtRoomTypeId.ReadOnly = true;
            this.txtRoomTypeId.Size = new System.Drawing.Size(231, 20);
            this.txtRoomTypeId.TabIndex = 4;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(23, 21);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(25, 19);
            this.label7.TabIndex = 2;
            this.label7.Text = "ID";
            // 
            // txtRoomTypeName
            // 
            this.txtRoomTypeName.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.txtRoomTypeName.Location = new System.Drawing.Point(159, 60);
            this.txtRoomTypeName.Name = "txtRoomTypeName";
            this.txtRoomTypeName.Size = new System.Drawing.Size(231, 20);
            this.txtRoomTypeName.TabIndex = 5;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(23, 61);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(96, 19);
            this.label8.TabIndex = 3;
            this.label8.Text = "Loại phòng";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.Transparent;
            this.label19.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Red;
            this.label19.Location = new System.Drawing.Point(185, 24);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(205, 22);
            this.label19.TabIndex = 5;
            this.label19.Text = "Quản Lý Khách hàng";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.Transparent;
            this.label20.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Red;
            this.label20.Location = new System.Drawing.Point(232, 17);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(252, 22);
            this.label20.TabIndex = 6;
            this.label20.Text = "Quản Lý Danh mục phòng";
            // 
            // Admin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(752, 464);
            this.Controls.Add(this.tcAdmin);
            this.Name = "Admin";
            this.Text = "Admin";
            this.tcAdmin.ResumeLayout(false);
            this.tpBill.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtgvBill1)).EndInit();
            this.tpRoomCategory.ResumeLayout(false);
            this.tpRoomCategory.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgvRoomCategory)).EndInit();
            this.panel11.ResumeLayout(false);
            this.tpFood.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nmFoodPrice)).EndInit();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtgvFood)).EndInit();
            this.tpCustomer.ResumeLayout(false);
            this.panel22.ResumeLayout(false);
            this.panel24.ResumeLayout(false);
            this.panel24.PerformLayout();
            this.panel25.ResumeLayout(false);
            this.panel25.PerformLayout();
            this.panel26.ResumeLayout(false);
            this.panel26.PerformLayout();
            this.panel27.ResumeLayout(false);
            this.panel28.ResumeLayout(false);
            this.panel28.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgvCustomer)).EndInit();
            this.tpTable.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgvTable)).EndInit();
            this.tpRole.ResumeLayout(false);
            this.tpRole.PerformLayout();
            this.panel12.ResumeLayout(false);
            this.panel12.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.TabControl tcAdmin;
        private System.Windows.Forms.TabPage tpBill;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ComboBox cbExportExcel;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnExport;
        private System.Windows.Forms.Button btnViewBill1;
        private System.Windows.Forms.DateTimePicker dtpkToDate1;
        private System.Windows.Forms.DateTimePicker dtpkFromDate1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridView dtgvBill1;
        private System.Windows.Forms.TabPage tpRoomCategory;
        private System.Windows.Forms.TextBox txtCategoryName;
        private System.Windows.Forms.TextBox txtCategoryID;
        private System.Windows.Forms.DataGridView dtgvRoomCategory;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Button btnEditMenu;
        private System.Windows.Forms.Button btnAddMenu;
        private System.Windows.Forms.TabPage tpFood;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.NumericUpDown nmFoodPrice;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.ComboBox cbCategory;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.TextBox txbFoodName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.TextBox txbFoodID;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button btnEditFood;
        private System.Windows.Forms.Button btnDeleteFood;
        private System.Windows.Forms.Button btnAddFood;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.DataGridView dtgvFood;
        public System.Windows.Forms.TabPage tpCustomer;
        private System.Windows.Forms.Panel panel22;
        private System.Windows.Forms.Panel panel24;
        private System.Windows.Forms.ComboBox cbType;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Panel panel25;
        private System.Windows.Forms.TextBox txbDisplayName;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Panel panel26;
        private System.Windows.Forms.TextBox txbUserName;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Panel panel27;
        private System.Windows.Forms.Button btnEditCustomer;
        private System.Windows.Forms.Button btnDeleteCustomer;
        private System.Windows.Forms.Button btnAddCustomer;
        private System.Windows.Forms.Panel panel28;
        private System.Windows.Forms.DataGridView dtgvCustomer;
        private System.Windows.Forms.TabPage tpTable;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Button BtnEditTable;
        private System.Windows.Forms.Button BtnDeleteTable;
        private System.Windows.Forms.Button btnAddTable;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox cbTableStatus;
        private System.Windows.Forms.TextBox txtTableName;
        private System.Windows.Forms.TextBox txtTableId;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DataGridView dtgvTable;
        private System.Windows.Forms.TabPage tpRole;
        private System.Windows.Forms.ComboBox cbStaff;
        private System.Windows.Forms.ComboBox cbAdmin;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Button btnViewDetail;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.TextBox txtRoomTypeId;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtRoomTypeName;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
    }
}