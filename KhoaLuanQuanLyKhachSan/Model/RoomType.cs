﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KhoaLuanQuanLyKhachSan
{
    class RoomType
    {
        public int id { get; set; }
        public string typeName { get; set; }

        public RoomType(int id, string name)
        {
            this.id = id;
            this.typeName = name;
        }
        public RoomType(DataRow row)
        {
            this.id = (int)row["id"];
            this.typeName = row["typeName"].ToString();
        }
    }
}
