﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KhoaLuanQuanLyKhachSan.Model
{
    public class Admin
    {
        public int id { get; set; }
        public string userName { get; set; }
        public string hashpass { get; set; }
        public string adminName { get; set; }
        public Admin() { }
        public Admin(int id, string userName, string hashpass, string adminName)
        {
            this.id = id;
            this.userName = userName;
            this.hashpass = hashpass;
            this.adminName = adminName;
        }
        public Admin(DataRow row)
        {
            this.id = (int) row["id"];
            this.userName = row["userName"].ToString();
            this.hashpass = row["hashpass"].ToString();
            this.adminName = row["adminName"].ToString();
        }
    }
}
