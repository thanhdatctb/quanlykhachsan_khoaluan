﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KhoaLuanQuanLyKhachSan.Model
{
    public class Room
    {
        public int roomNumber { get; set; }
        public int roomTypeId { get; set; }
        public string descripiton { get; set; }
        public string imageLink { get; set; }
        public float area { get; set; }
        public bool status { get; set; }
        public float pricePernight { get; set; }

        public Room(int roomNumber, int roomTypeId, string descripiton, string imageLink, float area, bool status, float pricePernight)
        {
            this.roomNumber = roomNumber;
            this.roomTypeId = roomTypeId;
            this.descripiton = descripiton;
            this.imageLink = imageLink;
            this.area = area;
            this.status = status;
            this.pricePernight = pricePernight;
        }

        public Room()
        {
        }
        public Room(DataRow row)
        {
            this.roomNumber = (int)row["roomNumber"];
            this.roomTypeId = (int)row["roomTypeId"];
            this.descripiton = (string)row["descripiton"];
            this.imageLink = (string)row["imageLink"];
            this.area = float.Parse(row["area"].ToString());
            this.status = (bool)row["status"];
            this.pricePernight = float.Parse(row["pricePernight"].ToString());
        }
    }
}
