﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KhoaLuanQuanLyKhachSan.Model
{
    public class Customer
    {
        public int id { get; set; }
        public string userName { get; set; }
        public string hashpass { get; set; }  
        public string fullName { get; set; }
        public string gender { get; set; }
        public string customerNational { get; set; }
        public DateTime dob { get; set; }
        public string noiSinh { get; set; }
        public string DiaChi { get; set; }
        public string Phone { get; set; }
        public string mail { get; set; }
        public string IDCard { get; set; }
        public string IDPlace { get; set; }
        public string passport { get; set; }
        public string passportPlace { get; set; }
        public string img { get; set; }
        public float account { get; set; }

        public Customer(int id, string userName, string hashpass, string fullName, string gender, string customerNational, DateTime dob, string noiSinh, string diaChi, string phone, string mail, string iDCard, string iDPlace, string passport, string passportPlace, float account)
        {
            this.id = id;
            this.userName = userName;
            this.hashpass = hashpass;
            this.fullName = fullName;
            this.gender = gender;
            this.customerNational = customerNational;
            this.dob = dob;
            this.noiSinh = noiSinh;
            DiaChi = diaChi;
            Phone = phone;
            this.mail = mail;
            IDCard = iDCard;
            IDPlace = iDPlace;
            this.passport = passport;
            this.passportPlace = passportPlace;
            this.account = account;
        }
        public Customer() 
        {

            this.account = 0;
        }

        public Customer(int id, string userName, string hashpass, string fullName, string gender, string customerNational, DateTime dob, string noiSinh, string diaChi, string phone, string mail, float account)
        {
            this.id = id;
            this.userName = userName;
            this.hashpass = hashpass;
            this.fullName = fullName;
            this.gender = gender;
            this.customerNational = customerNational;
            this.dob = dob;
            this.noiSinh = noiSinh;
            DiaChi = diaChi;
            Phone = phone;
            this.mail = mail;
            this.account = account;
            
        }

        public Customer(int id, string userName, string hashpass, string fullName, string gender, string customerNational, DateTime dob, string noiSinh, string diaChi, string phone, string mail, string iDCard, string iDPlace, string passport, string passportPlace)
        {
            this.id = id;
            this.userName = userName;
            this.hashpass = hashpass;
            this.fullName = fullName;
            this.gender = gender;
            this.customerNational = customerNational;
            this.dob = dob;
            this.noiSinh = noiSinh;
            DiaChi = diaChi;
            Phone = phone;
            this.mail = mail;
            IDCard = iDCard;
            IDPlace = iDPlace;
            this.passport = passport;
            this.passportPlace = passportPlace;
            this.account = 0;
        }
        public Customer(string userName, string hashpass, string fullName, string gender, string customerNational, DateTime dob, string noiSinh, string diaChi, string phone, string mail, string iDCard, string iDPlace, string passport, string passportPlace)
        {
            this.userName = userName;
            this.hashpass = hashpass;
            this.fullName = fullName;
            this.gender = gender;
            this.customerNational = customerNational;
            this.dob = dob;
            this.noiSinh = noiSinh;
            DiaChi = diaChi;
            Phone = phone;
            this.mail = mail;
            IDCard = iDCard;
            IDPlace = iDPlace;
            this.passport = passport;
            this.passportPlace = passportPlace;
            this.account = 0;
        }
        public Customer(DataRow row)
        {
            this.id = (int)row["id"]; 
            this.userName = row["userName"].ToString();
            this.hashpass = row["hashpass"].ToString();
            this.fullName = row["fullName"].ToString();
            this.gender = row["gender"].ToString();
            this.customerNational = row["customerNational"].ToString();
            this.dob = (DateTime)row["dob"];
            this.noiSinh = row["noiSinh"].ToString();
            this.DiaChi = row["DiaChi"].ToString();
            this.Phone = row["Phone"].ToString();
            this.mail = row["mail"].ToString();  
            this.IDCard = row["IDCard"].ToString();  
            this.IDPlace = row["IDPlace"].ToString();
            this.passport = row["passport"].ToString();  
            this.passportPlace = row["passportPlace"].ToString();
            this.account = float.Parse(row["account"].ToString());
            this.img = row["img"].ToString();
        }
    }
}
