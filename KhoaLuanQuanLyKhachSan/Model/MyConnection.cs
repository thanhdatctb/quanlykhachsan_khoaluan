﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace KhoaLuanQuanLyKhachSan.Model
{
    static class MyConnection
    {
        private static string connectionString;
        static SqlConnection  conn = GetConnection();
        
        public static SqlConnection GetConnection()
        {
            if(conn == null)//Nếu kết nối rỗng
            {
                connectionString = ConfigurationManager.AppSettings["ConnectionString"]; //Lấy connectionString từ app.config
                conn = new SqlConnection(connectionString);//Tạo ra 1 kết nối bằng connectionString
            }    
            return conn;//Trả về kết nối
        }
        public static SqlCommand GetCommand(string sql)
        {
            //conn = GetConnection();
            return new SqlCommand(sql, conn);
        }
        
        public static SqlDataReader ExcuteQueryWithId(string table, int id)//Hàm này lấy giá trị của 1 bảng theo id
        {
            //conn = G
            conn.Open();//Mở kết nối
            String sql = "select * from " + table + " where id = @id";//tạo câu lệnh select từ bảng và id
            SqlCommand cmd = GetCommand(sql);//Tạo ra đối tượng sqlCommand từ câu lệnh sql
            cmd.Parameters.Add("@id", id);// gán giá trị cho tham số @id là id
            SqlDataReader reader = cmd.ExecuteReader();//Thực thi đối tượng sqlCommand. đưa kết quả thực thi vào biến reader
            SqlDataReader result = null;//Khai báo biến result rỗng
            while(reader.Read())//ĐỌc từng dòng trong kết biến reader
            {
                result = reader;//Gán biến result
                break;//thoát khỏi vòng lặp (chỉ lấy dòng đầu tiên vì lấy theo id chỉ có 1 bản ghi duy nhất)
            }
            
            return result;//Trả về biến result đó
        }


       
        public static DataTable ExecuteQuery(string query, object[] parameter = null)//hàm này thực thi những câu lệnh sql trả về giá trị (thường là câu lệnh select)
        {
            DataTable data = new DataTable();//Tạo ra 1 đối tượng dataTable 

            using (SqlConnection connection = new SqlConnection(connectionString))//Tạo ra connection
            {

                connection.Open();//Mở kết nối đến csdl

                SqlCommand command = new SqlCommand(query, connection);//Tạo ra đối tượng SqlCommand từ câu lệnh sql và connection

                if (parameter != null)//nếu paramneter ko rỗng
                {
                    string[] listPara = query.Split(' ');//Tách chuỗi theo dấu cách  để lấy ra các parametter trong câu lệnh sql 
                    int i = 0;

                   
                    foreach (string item in listPara)
                    {
                        if (item.Contains('@'))//Nếu phần tử được tách từ chuỗi có kí tự '@'
                        {
                            command.Parameters.AddWithValue(item, parameter[i]);//Gán giá trị tương ứng theo parameter
                            i++;//đếm số biến của câu lệnh sql 
                        }
                    }
                }

                SqlDataAdapter adapter = new SqlDataAdapter(command);//Tạo ra đối tượng sqlDataAdapter từ sqlCommand

                adapter.Fill(data);//Đổ dữ liệu thực thi vào bảng data (DataTable)

                connection.Close();//Đóng kết nối

            }

            return data;
        }
        public static int ExecuteNonQuery(string query, object[] parameter = null)//Hàm này thực thi câu lệnh không trả về bản ghi (thường là insert, update)
        {
            //Xử lý tương tự như hàm phía trên
            int data = 0;

            using (SqlConnection connection = new SqlConnection(connectionString))
            {

                connection.Open();

                SqlCommand command = new SqlCommand(query, connection);

                if (parameter != null)
                {
                    string[] listPara = query.Split(' ');
                    int i = 0;
                    foreach (string item in listPara)
                    {
                        if (item.Contains('@'))
                        {
                            //command.Parameters.AddWithValue(item, parameter[i]);
                            command.Parameters.Add(item, parameter[i]);
                            i++;
                        }
                    }
                }

                //có 1 chút khác biệt khi sử dụng ExcuteNonQuery
                data = command.ExecuteNonQuery();//Hàm trả về số cột bị ảnh hưởng

                connection.Close();

            }

            return data;
        }

        public static object ExcuteScalar(string query, object[] parameter = null)//Hàm này trả về dòng đầu tiên của câu lệnh select
        {

            //Cách xử lý giống hệt hàm ExecuteQuery
            object data = 0;

            using (SqlConnection connection = new SqlConnection(connectionString))
            {

                connection.Open();

                SqlCommand command = new SqlCommand(query, connection);

                if (parameter != null)
                {
                    string[] listPara = query.Split(' ');
                    int i = 0;
                    foreach (string item in listPara)
                    {
                        if (item.Contains('@'))
                        {
                            command.Parameters.AddWithValue(item, parameter[i]);
                            i++;
                        }
                    }
                }
                data = command.ExecuteScalar();

                connection.Close();

            }

            return data;
        }
        public static void ExecuteNonQuery(SqlCommand cmd)
        {
            
        }
    }
}
