﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KhoaLuanQuanLyKhachSan.Controller
{
    class FileController
    {

        //Hàm tạo copy file ảnh người dùng upload vào thư mục Resource\Image\avatar
        public bool MoveFileToImageFolder(string sourceFile)//Tham số truyền vào: Đường dẫn của ảnh
        {
            try 
            {
                string targetPath;
                string relativePath = (new System.Uri(Assembly.GetEntryAssembly().CodeBase)).AbsolutePath;//Lấy đường dẫn tương đối của  project
                var FileName = sourceFile.Split('\\').Last();//Tách lấy tên file ảnh người dùng upload
                targetPath = @"..\..\Resource\image\avatar\" + FileName;//Tạo ô nhớ để lưu ảnh người dùng upload 
                System.IO.File.Copy(sourceFile, targetPath, true);//Copy ảnh người dùng vừa upload vào vùng nhớ vừa tạo
                return true;//Trả về true
            } catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
            
        }
        public string GetImgeLink(string dbLink)
        {
            return @"..\..\Resource\image\avatar\" + dbLink;
        }
    }
}
