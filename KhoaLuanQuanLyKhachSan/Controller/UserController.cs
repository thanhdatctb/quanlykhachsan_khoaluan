﻿using KhoaLuanQuanLyKhachSan.Model;
using KhoaLuanQuanLyKhachSan.Session;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KhoaLuanQuanLyKhachSan.Controller
{
    abstract class UserController
    {
        public int? LogIn(String username, string password, string table)
        {
            string hashPassword = PasswordController.Sha1Hash(password);//Tạo ra hashpass bằng cách băm password bằng thuật toán sha1
            String sql = "select id from " + table+ " where username = @username and hashpass = @hashpass";//tạo ra câu lệnh sql chọn id từ bảng trong db vs username = @username và hashpass = @hashpass
            SqlCommand cmd = MyConnection.GetCommand(sql);//Tạo ra 1 đối tượng SqlCommand  từ câu lệnh sql vừa tạo
            cmd.Parameters.Add("@username", username);//Gán giá trị cho biến @username trong SqlCommand
            cmd.Parameters.Add("@hashpass", hashPassword);//Gán giá trị cho biến @hashpass trong SqlCommand
            MyConnection.GetConnection().Open();//Mở kết nối đến database
            var reader = cmd.ExecuteReader();//Thực thi đối tượng SqlCommand và lưu giá trị vào biến reader
            int? id = null;//Tạo ra 1 biến id rỗng
            while (reader.Read())//ĐỌc từng thành phần trong biến reader
            {
                id = reader.GetInt32(0);//Gán giá trị cho id là cột thứ 0 trong bảng dữ liệu vừa lấy được
            }
            MyConnection.GetConnection().Close();//Đóng kết nối
            return id;//Trả về id
        }
    }
}
