﻿using KhoaLuanQuanLyKhachSan.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KhoaLuanQuanLyKhachSan.Controller
{
    class RoomController
    {
        public List<Room> GetAll()
        {
            List<Room> rooms = new List<Room>();
            string sql = "select * from rooms";
            var table = MyConnection.ExecuteQuery(sql);
            foreach(DataRow row in table.Rows)
            {
                rooms.Add(new Room(row));
            }    
            return rooms;
        }
    }
}
