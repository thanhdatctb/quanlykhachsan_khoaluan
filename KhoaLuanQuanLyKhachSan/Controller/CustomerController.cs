﻿using KhoaLuanQuanLyKhachSan.Model;
using KhoaLuanQuanLyKhachSan.Session;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KhoaLuanQuanLyKhachSan.Controller
{
    class CustomerController: UserController
    {
        public bool Add(Customer customer)
        {

            string query = @"insert into customers (userName,hashpass,fullName,gender,customerNational,
                            dob,noiSinh,DiaChi,Phone,mail,IDCard,IDPlace,passport,passportPlace,account, img, status) values
                            ( @username , @hashpass , @fullName , @gender , @customerNational , 
                            @dob , @noiSinh , @DiaChi , @Phone , @mail , @IDCard , @IDPlace , @passport , @passportPlace , @account , @img ,1)";
            MyConnection.ExecuteNonQuery(query, new object[]
            {
                customer.userName,
                customer.hashpass,
                customer.fullName,
                customer.gender,
                customer.customerNational,
                customer.dob,
                customer.noiSinh,
                customer.DiaChi,
                customer.Phone,
                customer.mail,
                customer.IDCard,
                customer.IDPlace,
                customer.passport,
                customer.passportPlace,
                customer.account,
                customer.img
            }) ;
            
            return true;
        }

        public bool Delete(object ob)
        {
            throw new NotImplementedException();
        }

        public bool Edit(Customer customer)
        {
            string query = @"update customers set 
                               
                            username = @username ,
                            hashpass = @hashpass ,
                            fullName = @fullName ,
                            gender = @gender ,
                            customerNational = @customerNational  ,
                           dob = @dob ,
                           noiSinh = @noiSinh ,
                           DiaChi = @DiaChi ,
                           Phone = @Phone ,
                           mail = @mail ,
                           IDCard = @IDCard ,
                           IDPlace = @IDPlace ,
                           passport = @passport ,
                           passportPlace = @passportPlace ,
                           account = @account ,
                           img = @img 
                            where id = @Id";
            MyConnection.ExecuteNonQuery(query, new object[]
            {
                customer.userName,
                customer.hashpass,
                customer.fullName,
                customer.gender,
                customer.customerNational,
                customer.dob,
                customer.noiSinh,
                customer.DiaChi,
                customer.Phone,
                customer.mail,
                customer.IDCard,
                customer.IDPlace,
                customer.passport,
                customer.passportPlace,
                customer.account,
                customer.img,
                customer.id
            }) ;
            return true;
        }

        public List<Customer> GetAll()
        {
            List<Customer> customers = new List<Customer>();
            string sql = @"select * from customers where status = 1";
            DataTable table = MyConnection.ExecuteQuery(sql);
            foreach(DataRow row in table.Rows)
            {
                customers.Add(new Customer(row));
            }    
            return customers;
        }

        public Customer GetById(int id)
        {
            Customer customer = new Customer();//tạo ra 1 customer rỗng
                                               //var data = MyConnection.ExcuteQueryWithId("customers", id);//Truy vấn vào DB để lấy bản ghi có id phù hợp bằng hàm ExcuteQueryWithId của lớp MyConnection
                                               //String userName = data.GetString(1);// gán username bằng giá trị cột 1 của bảng vừa lấy được
                                               //String hashpass = data.GetString(2);// tương tự
                                               //String fullName = data.GetString(3);
                                               //string gender =  data.GetString(4);
                                               //String customerNational = data.GetString(5);
                                               //DateTime dob = data.GetDateTime(6);
                                               //String noiSinh = data.GetString(7);
                                               //String DiaChi = data.GetString(8);
                                               //String Phone = data.GetString(9);
                                               //String mail = data.GetString(10);
                                               //String IDCard = data.GetString(11);
                                               //String IDPlace = data.GetString(12);
                                               //String passport = data.GetString(13);
                                               //String passportPlace = data.GetString(14);
                                               //float account = (float)data.GetDouble(15);




            //Tạo ra 1 customer với các dữ liệu vừa lấy được
            //customer = new Customer(id, userName, hashpass, fullName, gender, customerNational, dob, noiSinh, DiaChi, Phone, mail, account);
            //customer.IDCard = IDCard;
            //customer.IDPlace = IDPlace;
            //customer.passport = passport;
            //customer.passportPlace = passportPlace;
            // MyConnection.GetConnection().Close();//Đóng kết nối

            string sql = "select * from customers where id = @Id";
            var table = MyConnection.ExecuteQuery(sql, new object[] { id });
            customer = new Customer(table.Rows[0]);
            return customer;// trả về customer vừa tạo bên trên
        }

        public bool LogIn(String username, string password)
        {
            //Lấy ID của customer từ bảng customers trong db bằng  hàm login của lớp UserController
            int? id = base.LogIn(username, password, "customers");
            if(id == null)//Nếu id rỗng
            {
                return false; // Trả về false
            }
            //Nếu id không rỗng
            int idint = (int)id;// chuyển id từ kiểu int? thành int
            Customer customer = this.GetById(idint);//lấy customer theo id băng hàm getBYId vừa tạo bên trên
            MySession.customer = customer;//Đưa customer vừa tạo vào session
            return true;//trả về true
        }
        public bool Delete(int id)
        {
            string sql = "update customers set status = 0 where id = @id";
            MyConnection.ExecuteNonQuery(sql, new object[] { id });
            return true;
        }
    }
}
