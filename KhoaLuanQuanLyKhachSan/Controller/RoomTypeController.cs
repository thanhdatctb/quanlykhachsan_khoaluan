﻿using KhoaLuanQuanLyKhachSan.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KhoaLuanQuanLyKhachSan.Controller
{
    class RoomTypeController
    {
        public bool Add(RoomType roomType)
        {
            string query = "insert into roomTypes (typeName) values ( @name )";
            MyConnection.ExecuteNonQuery(query, new object[] {roomType.typeName} );
            return true;
        }

        public List<RoomType> GetAll()
        {
            List<RoomType> roomTypes = new List<RoomType>();
            string sql = "select * from roomTypes";
            var table = MyConnection.ExecuteQuery(sql);
            foreach(DataRow row in table.Rows)
            {
                roomTypes.Add(new RoomType(row));
            }    
            return roomTypes;
        }
    }
}
