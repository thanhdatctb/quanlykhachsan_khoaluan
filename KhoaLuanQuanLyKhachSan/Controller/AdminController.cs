﻿using KhoaLuanQuanLyKhachSan.Model;
using KhoaLuanQuanLyKhachSan.Session;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KhoaLuanQuanLyKhachSan.Controller
{
   class AdminController : UserController
    {
       
        public bool LogIn(String username, string password)
        {
            var id = base.LogIn(username,password,"admins");//Lấy ID của admin từ bảng admins trong db bằng  hàm login của lớp UserController
            if(id == null)//Nếu id rỗng
            {
                return false;//Trả về false;
            }
            //nếu id k rỗng
            int idint = (int)id;//chuyển kiểu int? thành int
            Admin admin = this.GetById(idint);//Lấy admin theo id bằng hàm GetById
            MySession.admin = admin;//Lưu admin vừa lấy được vào session

            return true;// Trả về true
        }
        public Admin GetById(int id)
        {
            var data = MyConnection.ExcuteQueryWithId("admins", id);//Truy vấn vào DB để lấy bản ghi có id phù hợp bằng hàm ExcuteQueryWithId
            string userName = data.GetString(1);//Gán userName bằng cột thứ 1 của bản ghi vừa lấy được
            string hashpass = data.GetString(2);//Gán hashpass bằng cột thứ 2 của bản ghi vừa lấy được
            string adminName = data.GetString(3);//Gán adminName bằng cột thứ 3 của bản ghi vừa lấy được
            Admin admin = new Admin(id, userName, hashpass, adminName);// Tạo ra 1 đối tượng admin mới với những thông tin vừa lấy được
            return admin;//Trả về admin vừa tạo
        }

       
    }
}
